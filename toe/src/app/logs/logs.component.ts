import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {GameService} from "../game.service";
import {Game} from "../models/game";
import {Movement} from "../models/movement";
import {SharedService} from "../shared/shared.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit, OnDestroy {

  currentArray = [];
  moves: Movement[] = [];
  player1Name: string = "";
  player2Name: string = "";


  @Input() playerName: string = '';

  @Input() move: Movement[] = [{
    player: '',
    positions: []
  }]

  getMoveSubscription: Subscription | undefined;

  constructor(
    private gameService: GameService,
    private shared: SharedService) {
  }

  ngOnInit(): void {
    this.getMoves();
    this.getMoveSubscription = this.shared.getMoveList().subscribe(move => {
      // this.move=move;
      console.log(move);
      this.getMoves();
    })
  }

  getMoves() {
    this.gameService.getCurrentGame().subscribe((response: any) => {
        if (response[0]) {
          this.player1Name = response[0].player1Name;
          this.player2Name = response[0].player2Name;
          if (response[0].movements !== null) {
            this.moves = response[0].movements;
          }
        }

      }
    );
  }

  checkName(name: string): string {
    let playerName = "";
    if (name === "1") {
      playerName = this.player1Name;
    } else if (name === "2") {
      playerName = this.player2Name;
    }
    return playerName;
  }

  checkSymbol(sym: string) {
    return sym === "1" ? "X" : "O";
  }

  viewPreviousState(num: number) {
    console.log(num)
    console.log(this.moves.slice(0, num + 1))
    this.shared.updateTempArray(this.moves.slice(0, num + 1));
  }

  ngOnDestroy() {
    if (this.getMoveSubscription) {
      this.getMoveSubscription.unsubscribe();
    }
  }

}
