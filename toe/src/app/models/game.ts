import {Movement} from "./movement";

export interface Game {
  id: string;
  player1Name: string;
  player2Name: string;
  movements : Movement
  status: string;
}
