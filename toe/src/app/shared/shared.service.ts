import {Injectable} from "@angular/core";
import {Movement} from "../models/movement";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class SharedService {
  private tempArray = new BehaviorSubject<Movement[]>([]);
  private moveList = new BehaviorSubject<Movement[]>([])
  private startPlayer = new BehaviorSubject<string>("");

  constructor() {
  }

  updateStartingPlayer(id: string){
    this.startPlayer.next(id);
  }

  updateTempArray(val:Movement[]){
    this.tempArray.next(val);
  }

  updateMoveList(val:Movement[]){
    this.moveList.next(val);
  }

  getStartingPlayer(): Observable<string>{
    return this.startPlayer.asObservable();
  }

  getMoveList(): Observable<Movement[]>{
    return this.moveList.asObservable();
  }

  getTempArray(): Observable<Movement[]>{
    return this.tempArray.asObservable();
  }


}
